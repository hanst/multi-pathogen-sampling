use std::env::args_os;
use std::fs::{read_to_string, write};
use std::hash::{Hash, Hasher};
use std::sync::Mutex;

use hashbrown::{hash_map::Entry, HashMap, HashSet};
use once_cell::sync::Lazy;
use serde::{Deserialize, Deserializer, Serialize};
use toml::{from_str, to_string_pretty};

/// Describes a problem instance, i.e. a list of requirements and rules for matching equivalent matrices and reusable samples
#[derive(Debug, Deserialize)]
struct Problem {
    requirements: Vec<Requirement>,
    #[serde(default)]
    equivalent_matrices: Vec<EquivalentMatrices>,
    #[serde(default)]
    reusable_samples: Vec<ReusableSample>,
}

/// Describes a requirement for collecting a number of samples for a given pest and a given matrix within a certain time range
#[derive(Debug, Deserialize)]
struct Requirement {
    pest: LeakedString,
    matrix: LeakedString,
    samples: u32,
    from: u8,
    until: u8,
}

/// For the given set of pests, the listed matrices are considered equivalent
#[derive(Debug, Deserialize)]
struct EquivalentMatrices {
    pest: HashSet<String>,
    matrices: HashSet<String>,
}

/// For the given set of matrices, the listed pests can be tested for using a single sample
#[derive(Debug, Deserialize)]
struct ReusableSample {
    matrix: HashSet<String>,
    pests: HashSet<String>,
}

/// Describes a (partial) solution, i.e. list of actions
#[derive(Debug, Default, Clone, Serialize)]
struct Solution {
    actions: Vec<Action>,
}

impl Solution {
    /// Compute the number of time slots where a trip is planned
    fn trips(&self) -> u32 {
        // Insert the time slots into a bit mask to avoid temporary allocations.
        self.actions
            .iter()
            .fold(0_u64, |trips, action| trips | (1 << action.at))
            .count_ones()
    }

    /// Compute the number of samples collected over all actions
    fn samples(&self) -> u32 {
        self.actions.iter().map(|action| action.samples).sum()
    }
}

/// Describes an action, i.e. taking a number of samples for a given pest using a given matrix at a certain time slot
#[derive(Debug, Clone, Serialize)]
struct Action {
    pest: LeakedString,
    matrix: LeakedString,
    samples: u32,
    at: u8,
    /// The full amount of samples implied by the satisfied requirement to be able to convert the different sample numbers implied by distinct but equivalent matrices
    #[serde(skip)]
    reference: u32,
}

const USAGE: &str = "usage: <problem.toml> <solution.toml>";

fn main() {
    let mut args = args_os();
    let problem_path = args.nth(1).expect(USAGE);
    let solution_path = args.next().expect(USAGE);

    // Parse and validate TOML file describing the problem instance.
    let problem = from_str::<Problem>(&read_to_string(problem_path).unwrap()).unwrap();

    for requirement in &problem.requirements {
        assert!(requirement.from <= requirement.until);
        assert!(requirement.until < 64);
    }

    // The target function, i.e. minimize the number of trips and if those are equal, the overall number of samples.
    let target = |solution: &Solution| (solution.trips(), solution.samples());

    let best_solution = trivial_solution(&problem);
    let best_target_value = target(&best_solution);

    let mut solver = Solver {
        problem: &problem,
        solution: Default::default(),
        matches: Default::default(),
        target,
        best_target_value,
        best_solution,
    };

    solver.satisfy();

    // Write the solution to a TOML file.
    write(
        solution_path,
        to_string_pretty(&solver.best_solution).unwrap(),
    )
    .unwrap();
}

/// Construct the trivial solution, i.e. a separate sampling action for each requirement without considering overlap, equivalent matrices or reusable samples
fn trivial_solution(problem: &Problem) -> Solution {
    let mut solution = Solution::default();

    for requirement in &problem.requirements {
        solution.actions.push(Action {
            pest: requirement.pest,
            matrix: requirement.matrix,
            samples: requirement.samples,
            at: requirement.from,
            reference: requirement.samples,
        });
    }

    solution
}

/// A branch-and-bound solver for a given problem instance
struct Solver<'a, T, V> {
    /// Problem instance
    problem: &'a Problem,
    /// Candidate (partial) solution
    solution: Solution,
    /// Caches matching requirement and action combinations due to equivalent matrices and reusable samples
    matches: HashMap<Match, (bool, bool)>,
    /// Target function
    target: T,
    /// Value of the target function at the best known solution
    best_target_value: V,
    /// Best known solution
    best_solution: Solution,
}

impl<T, V> Solver<'_, T, V>
where
    T: Fn(&Solution) -> V,
    V: Ord,
{
    /// Main branch-and-bound routine using depth-first search
    fn satisfy(&mut self) {
        // Prune candidate (partial) solutions if their target value is already as bad as the best known solution.
        //
        // This implies that the target function must be optimisic, i.e. its value for a partial solution must always
        // be lower than its value for any completion of that partial solution. This is fulfilled for the number of trips
        // and samples as actions will only be added which can only increase the number of trips and samples.
        let target_value = (self.target)(&self.solution);
        if self.best_target_value <= target_value {
            return;
        }

        // Check if all requirements are satisfied by the aggregate effect of all actions planned so far.
        let mut all_requirements_satisfied = true;

        for requirement in &self.problem.requirements {
            let mut samples = requirement.samples;

            for action in &self.solution.actions {
                // Does the action fall into the required time range?
                if action.at < requirement.from || action.at > requirement.until {
                    continue;
                }

                // Do pest and matrix match, possibly after considering equivalent matrices and reusable samples?
                let (matches, equivalent_matrices) =
                    matches(&mut self.matches, self.problem, requirement, action);

                if !matches {
                    continue;
                }

                // Ajdust the number of samples if distinct but equivalent matrices are used.
                let equivalent_samples = if equivalent_matrices {
                    action.samples * requirement.samples / action.reference
                } else {
                    action.samples
                };

                samples = samples.saturating_sub(equivalent_samples);

                if samples == 0 {
                    break;
                }
            }

            // The number of matching samples is not sufficient and an additional action is required.
            if samples != 0 {
                all_requirements_satisfied = false;

                // Try all possible time slots for adding the minimal action satisfying
                // the (rest of the) given requirement by recursively invoking the solver.
                for at in requirement.from..=requirement.until {
                    self.solution.actions.push(Action {
                        pest: requirement.pest,
                        matrix: requirement.matrix,
                        samples,
                        at,
                        reference: requirement.samples,
                    });

                    self.satisfy();

                    self.solution.actions.pop().unwrap();
                }
            }
        }

        // If all requirements are indeed satisfied, this is a complete solution and
        // we check whether it is better than the current best known solution.
        if all_requirements_satisfied && self.best_target_value > target_value {
            self.best_target_value = target_value;
            self.best_solution = self.solution.clone();
        }
    }
}

/// Caches the results of matching a requirement and an action based on equivalent matrices and reusable samples
fn matches(
    matches: &mut HashMap<Match, (bool, bool)>,
    problem: &Problem,
    requirement: &Requirement,
    action: &Action,
) -> (bool, bool) {
    let match_ = Match {
        requirement_pest: requirement.pest,
        requirement_matrix: requirement.matrix,
        action_pest: action.pest,
        action_matrix: action.matrix,
    };

    match matches.entry(match_) {
        Entry::Occupied(entry) => *entry.get(),
        Entry::Vacant(entry) => {
            let same_pest = requirement.pest == action.pest;
            let same_matrix = requirement.matrix == action.matrix;

            // Does the pest implied by the action have equivalent matrices matching the requirement?
            let equivalent_matrices = !same_matrix
                && problem
                    .equivalent_matrices
                    .iter()
                    .any(|equivalent_matrices| {
                        equivalent_matrices.pest.contains(action.pest.0)
                            && equivalent_matrices.matrices.contains(action.matrix.0)
                            && equivalent_matrices.matrices.contains(requirement.matrix.0)
                    });

            // Is the matrix implied by the action resuable for sampling a pest matching the requirement?
            let reusable_sample = !same_pest
                && problem.reusable_samples.iter().any(|reusable_sample| {
                    reusable_sample.matrix.contains(action.matrix.0)
                        && reusable_sample.pests.contains(action.pest.0)
                        && reusable_sample.pests.contains(requirement.pest.0)
                });

            let matches = (same_pest || reusable_sample) && (same_matrix || equivalent_matrices);

            *entry.insert((matches, equivalent_matrices))
        }
    }
}

/// A combination of pest and matrix of a requirement matching pest and matrix of an action
#[derive(Debug, PartialEq, Eq, Hash)]
struct Match {
    requirement_pest: LeakedString,
    requirement_matrix: LeakedString,
    action_pest: LeakedString,
    action_matrix: LeakedString,
}

/// A string that is very cheap to compare and copy as it is interned and never deallocated until the program ends
#[derive(Debug, Clone, Copy, Eq, Serialize)]
struct LeakedString(&'static str);

impl PartialEq for LeakedString {
    fn eq(&self, other: &Self) -> bool {
        let self_ = self.0.as_ptr() as usize;
        let other = other.0.as_ptr() as usize;

        self_ == other
    }
}

impl Hash for LeakedString {
    fn hash<H>(&self, hasher: &mut H)
    where
        H: Hasher,
    {
        let self_ = self.0.as_ptr() as usize;

        hasher.write_usize(self_);
    }
}

impl<'de> Deserialize<'de> for LeakedString {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let inner = String::deserialize(deserializer)?;

        static VALUES: Lazy<Mutex<HashSet<&'static str>>> =
            Lazy::new(|| Mutex::new(HashSet::new()));

        let mut values = VALUES.lock().unwrap();

        if let Some(value) = values.get(inner.as_str()) {
            return Ok(Self(value));
        }

        let value = Box::leak(inner.into_boxed_str());

        values.insert(value);

        Ok(Self(value))
    }
}
